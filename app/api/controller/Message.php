<?php
namespace app\api\controller;

use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use app\common\model\Message as MessageModel;
use app\common\model\MessageTo;

class Message
{	
	//send msg
	public function sendMsg($sendId,$receveId,$data)
	{
		$msg = MessageModel::create($data);
		$msgId = $msg->id;
		$result = MessageTo::create(['send_id'=>$sendId,'receve_id'=>$receveId,'message_id'=>$msgId]);
		if($result){
			return true;
		}
    }
	
	//receve msg
	public function receveMsg($uid)
	{
		 $msg = Db::name('message_to')
		->alias('t')
		->join('message m','t.message_id = m.id' )
		->join('user u','t.send_id = u.id')
		->field('t.id as id,name,title,link,receve_id,t.create_time as create_time,is_read')
		->where('t.receve_id',$uid)
		->where(['t.delete_time'=>0])
		->order(['t.create_time'=>'desc'])
		->select();
		return $msg;
    }

}